/*
 * For Master's Thesis research
 * Igor Gursky (gdarkey@gmail.com)
 * Copyright (c) 2014./2015.
 */

package lv.rtu.cna;

import org.jbpm.test.JbpmJUnitBaseTestCase;
import org.jbpm.test.JbpmJUnitTestCase;
import org.junit.Test;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.process.ProcessInstance;
import org.kie.api.runtime.process.WorkItem;
import org.kie.internal.KnowledgeBase;
import org.kie.internal.builder.KnowledgeBuilder;
import org.kie.internal.runtime.StatefulKnowledgeSession;

import java.util.HashMap;
import java.util.Map;

import static org.kie.internal.builder.KnowledgeBuilderFactory.newKnowledgeBuilder;
import static org.kie.internal.io.ResourceFactory.newClassPathResource;

public class ServiceRankingBPMNProcessTest extends JbpmJUnitBaseTestCase {
    public ServiceRankingBPMNProcessTest() {
        super(true, true);
    }

    @Test
    public void launchBusinessProcess() {
        Map<String, Object> parameters = new HashMap<>();
        KnowledgeBuilder kbuilder = newKnowledgeBuilder();
        kbuilder.add(newClassPathResource("lv/rtu/cna/Service Ranking.bpmn2"), ResourceType.BPMN2); //pievienojam testam biznesa procesu
        kbuilder.add(newClassPathResource("lv/rtu/cna/promotionrules.drl"), ResourceType.DRL); //pievienojam Biznesa likumu kopu

        KnowledgeBase kbase = kbuilder.newKnowledgeBase();
        StatefulKnowledgeSession ksession = kbase.newStatefulKnowledgeSession();
        JbpmJUnitTestCase.TestWorkItemHandler handler = new JbpmJUnitTestCase.TestWorkItemHandler();
        ksession.getWorkItemManager().registerWorkItemHandler("Human Task", handler);

        ksession.getWorkItemManager().registerWorkItemHandler("HighlightServices", new HighlightingHandler(ksession)); //piereģistrējam servisa izcelšanas servisu
        ksession.getWorkItemManager().registerWorkItemHandler("GenerateReport", new ReportGeneratorHandler()); //piereģistrējam atskaites ģenerēšanas servisu

        parameters.put("Automated", "true"); //vienīgas globālais mainīgajs, kas pilnīgi automatizē procesu

        ProcessInstance processInstance = ksession.createProcessInstance("cna.ServiceRanking", parameters);
        ksession.insert(processInstance);

        ksession.startProcessInstance(processInstance.getId()); //process tiek palaists
        assertTrue(processInstance.getState() == ProcessInstance.STATE_ACTIVE); //pārbaudam ka statuss ir ACTIVE

        ksession.fireAllRules(); //tiek pārbaudīti biznesa likumi
        WorkItem workItem = handler.getWorkItem();
        if (workItem != null) {
            ksession.getWorkItemManager().completeWorkItem(workItem.getId(), null);
        }
        assertProcessInstanceCompleted(processInstance.getId(), ksession); //pārbaudam ka process ir pabeigts
    }
}