/*
 * For Master's Thesis research
 * Igor Gursky (gdarkey@gmail.com)
 * Copyright (c) 2014./2015.
 */

package lv.rtu.cna;

import lv.rtu.cna.beans.RankedInstance;
import lv.rtu.cna.beans.RankedInstanceData;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.context.RequestScoped;
import java.util.List;

import static lv.rtu.cna.other.Enums.URL.FORCE_RANKING;
import static lv.rtu.cna.other.Enums.URL.REPORT;
import static lv.rtu.cna.other.Enums.URL.INSTANCES_RANKED;
import static lv.rtu.cna.other.Utils.makeCall;
import static lv.rtu.cna.other.Utils.readJSONData;
import static lv.rtu.cna.other.Utils.readJSONStatus;

@RequestScoped
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class RankingService {
    public static RankedInstanceData performRanking() throws Exception {
        Integer status = readJSONStatus(makeCall(FORCE_RANKING));
        if (status.equals(200)) {
            String response = makeCall(INSTANCES_RANKED);
            List<RankedInstance> rankedInstances = (List<RankedInstance>) readJSONData(response, RankedInstance.class);
            return new RankedInstanceData(rankedInstances);
        }
        return null;
    }

    public static void generateReport() throws Exception {
        Integer status = readJSONStatus(makeCall(REPORT));
        if (!status.equals(200)) {
            throw new Exception("Unable to generate report! Wrong status code received from backend: " + status);
        }
    }
}