/*
 * For Master's Thesis research
 * Igor Gursky (gdarkey@gmail.com)
 * Copyright (c) 2014./2015.
 */

package lv.rtu.cna;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemHandler;
import org.kie.api.runtime.process.WorkItemManager;

import javax.enterprise.context.RequestScoped;

import static lv.rtu.cna.RankingService.generateReport;

@RequestScoped
public class ReportGeneratorHandler implements WorkItemHandler {

    public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
        try {
            generateReport();
        } catch (Exception e) {
            e.printStackTrace();
        }
        manager.completeWorkItem(workItem.getId(), null);
    }

    public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
        //REST call cannot be aborted
    }
}