/*
 * For Master's Thesis research
 * Igor Gursky (gdarkey@gmail.com)
 * Copyright (c) 2014./2015.
 */

package lv.rtu.cna;

import lv.rtu.cna.beans.RankedInstanceData;
import org.jbpm.workflow.instance.node.WorkItemNodeInstance;
import org.kie.api.runtime.process.NodeInstance;
import org.kie.api.runtime.process.NodeInstanceContainer;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemHandler;
import org.kie.api.runtime.process.WorkItemManager;
import org.kie.api.runtime.process.WorkflowProcessInstance;
import org.kie.internal.runtime.StatefulKnowledgeSession;

import static lv.rtu.cna.other.Utils.doHighlighting;

public class HighlightingHandler implements WorkItemHandler {
    private StatefulKnowledgeSession ksession;

    public HighlightingHandler(StatefulKnowledgeSession ksession) {
        this.ksession = ksession;
    }

    public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
        WorkflowProcessInstance processInstance = (WorkflowProcessInstance)
                ksession.getProcessInstance(workItem.getProcessInstanceId());
        WorkItemNodeInstance nodeInstance = findNodeInstance(workItem.getId(), processInstance);
        RankedInstanceData data = (RankedInstanceData) nodeInstance.getVariable("rankedInstances");

        try {
            doHighlighting(data.getRankedInstances());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        manager.completeWorkItem(workItem.getId(), null);
    }

    public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
        //REST call cannot be aborted
    }

    private WorkItemNodeInstance findNodeInstance(long workItemId, NodeInstanceContainer container) {
        for (NodeInstance nodeInstance : container.getNodeInstances()) {
            WorkItemNodeInstance workItemNodeInstance = (WorkItemNodeInstance) nodeInstance;
            if (workItemNodeInstance.getWorkItem().getId() == workItemId) {
                return workItemNodeInstance;
            }
        }
        return null;
    }
}