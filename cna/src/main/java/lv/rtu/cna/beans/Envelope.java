/*
 * For Master's Thesis research
 * Igor Gursky (gdarkey@gmail.com)
 * Copyright (c) 2014./2015.
 */

package lv.rtu.cna.beans;

public class Envelope {

    private Integer status;
    private Object data;

    public Integer getStatus() {
        return status;
    }

    public Object getData() {
        return data;
    }
}
