/*
 * For Master's Thesis research
 * Igor Gursky (gdarkey@gmail.com)
 * Copyright (c) 2014./2015.
 */

package lv.rtu.cna.beans;

import java.io.Serializable;

public class RankedInstance implements Serializable {
    private Integer serviceInstanceId;
    private Integer serviceId;
    private Integer municipalityId;
    private Double rank;
    private Boolean forcePromoted;

    public Integer getServiceInstanceId() {
        return serviceInstanceId;
    }

    public void setServiceInstanceId(Integer serviceInstanceId) {
        this.serviceInstanceId = serviceInstanceId;
    }

    public Boolean getForcePromoted() {
        return forcePromoted;
    }

    public void setForcePromoted(Boolean forcePromoted) {
        this.forcePromoted = forcePromoted;
    }

    public Integer getServiceId() {
        return serviceId;
    }

    public void setServiceId(Integer serviceId) {
        this.serviceId = serviceId;
    }

    public Integer getMunicipalityId() {
        return municipalityId;
    }

    public void setMunicipalityId(Integer municipalityId) {
        this.municipalityId = municipalityId;
    }

    public Double getRank() {
        return rank;
    }

    public void setRank(Double rank) {
        this.rank = rank;
    }
}
