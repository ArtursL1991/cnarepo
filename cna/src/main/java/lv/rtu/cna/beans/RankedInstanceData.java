/*
 * For Master's Thesis research
 * Igor Gursky (gdarkey@gmail.com)
 * Copyright (c) 2014./2015.
 */

package lv.rtu.cna.beans;

import java.io.Serializable;
import java.util.List;

public class RankedInstanceData implements Serializable {

    List<RankedInstance> rankedInstances;

    public List<RankedInstance> getRankedInstances() {
        return rankedInstances;
    }

    public RankedInstanceData() {
    }

    public RankedInstanceData(List<RankedInstance> rankedInstances) {
        this.rankedInstances = rankedInstances;
    }

    public void setRankedInstances(List<RankedInstance> rankedInstances) {
        this.rankedInstances = rankedInstances;
    }
}
