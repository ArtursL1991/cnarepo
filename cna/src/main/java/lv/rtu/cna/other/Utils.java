/*
 * For Master's Thesis research
 * Igor Gursky (gdarkey@gmail.com)
 * Copyright (c) 2014./2015.
 */

package lv.rtu.cna.other;

import lv.rtu.cna.beans.Envelope;
import lv.rtu.cna.beans.RankedInstance;
import lv.rtu.cna.other.Enums.HTTPMethods;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.type.JavaType;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static lv.rtu.cna.other.Enums.HTTPMethods.*;
import static lv.rtu.cna.other.Enums.URL.HIGHLIGHTING;

public class Utils {

    public static String makeCall(Enums.URL URL) throws InterruptedException {
        return makeCall(URL, GET, null);
    }

    public static String makeCall(Enums.URL URL, HTTPMethods method, String requestBody) throws InterruptedException {
        try {
            java.net.URL url = new URL(URL.getValue());
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setUseCaches(false);
            conn.setDoOutput(true);
            conn.setRequestMethod(method.toString());
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Content-Type", "application/json");

            if (requestBody != null) {
                byte[] stringInBytes = requestBody.getBytes("UTF-8");
                OutputStream os = conn.getOutputStream();
                os.write(stringInBytes);
                os.close();
            }

            try {
                if (conn.getResponseCode() != 200) {
                    System.out.println("Warning! HTTP response code is - " + conn.getResponseCode());
                }
            } catch (Exception e) {
                System.out.println("Waiting 30 seconds...");
                Thread.sleep(30 * 1000);
                System.out.println("Trying to reconnect...");
                makeCall(URL, method, requestBody);
            }
            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
            String result = "";
            String str;
            while ((str = br.readLine()) != null) {
                result += str;
            }
            conn.disconnect();
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Integer readJSONStatus(String input) throws Exception {
        ObjectMapper mapper = getObjectMapper();
        return mapper.readValue(input, Envelope.class).getStatus();
    }

    public static Object readJSONData(String input, Class<?> className) throws Exception {
        ObjectMapper mapper = getObjectMapper();
        Object obj = mapper.readValue(input, Envelope.class).getData();
        JavaType type = mapper.getTypeFactory().constructCollectionType(List.class, className);
        return mapper.convertValue(obj, type);
    }

    private static ObjectMapper getObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationConfig.Feature.INDENT_OUTPUT, true);
        mapper.configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES, false);
        mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, true);
        mapper.configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        return mapper;
    }

    public static List<Integer> getServiceIdsToPromote(List<RankedInstance> rankedInstances) {
        List<Integer> serviceIds = new ArrayList<>();
        for (RankedInstance instance : rankedInstances) {
            if (!instance.getForcePromoted()) continue;
            Integer serviceId = instance.getServiceId();
            if (!serviceIds.contains(serviceId)) serviceIds.add(serviceId);
        }
        return serviceIds;
    }

    public static void doHighlighting(List<RankedInstance> rankedInstances) throws InterruptedException {
        List<Integer> serviceIds = getServiceIdsToPromote(rankedInstances);
        makeCall(HIGHLIGHTING, HTTPMethods.PUT, serviceIds.toString());
    }
}
