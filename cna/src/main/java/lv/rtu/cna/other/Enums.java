/*
 * For Master's Thesis research
 * Igor Gursky (gdarkey@gmail.com)
 * Copyright (c) 2014./2015.
 */
package lv.rtu.cna.other;

public class Enums {

    private static String root = "http://localhost:8888/cna/rest";

    public enum HTTPMethods {
        GET,
        PUT
    }

    public enum URL {
        FORCE_RANKING("ranking/forced"),
        REPORT("ranking/report"),
        HIGHLIGHTING("highlighting"),
        INSTANCES_RANKED("instances/external/ranks");

        public final String value;

        private URL(String str) {
            this.value = str;
        }

        public String getValue() {
            return root + "/" + value;
        }
    }
}